randn('state', 4);

function num = from_db(db)
  num = 10 .^ (db/10);
end

% h -> matrix of MxN Rayleigh-distributed channel gains.
function H = channel(M, N)
  H = (randn(M,N) + randn(M,N)*i) / sqrt(2);
end


function Iw = leakage(P, d, H, U, V)
  K = size(U,2);
  Iw = 0;
  for j=1:K
    for k=1:K
      Iw = Iw + P(j)/d(j) * P(k)/d(k) * trace(U{k}' * H{k,j} * V{j}*V{j}' * H{k,j}' * U{k});
    end
  end
end

function C = capacity(H)
  C = 0;
end

K = 3; % number of users
nT = 2; % number of rx antennas
nR = 2; % number of tx antennas

M = nR*ones(1,K);
N = nT*ones(1,K);

P = ones(1,K);
H = cell(K,K);
d = min(M, N);

for i=1:K
  for j=1:K
    H{i,j} = channel(M(i), N(i));
%    H{j,i} = H{i,j}';
  end
end
[V,U] = iterative_alignment(M, N, P, H, d, 0.1);
H = reshape(cell2mat(H), [nR, nT, K, K]);

SNR = 0:40;
y = arrayfun(@(snr) MaximumSumRate2(U,H,V .* from_db(snr)), SNR);
hold on;
for i=1:size(SNR,2)
  [V,U] = maxSINR_algorithm(H, V0, SNR(i), 10);
  y(i) = MaximumSumRate2(U, H, V .* from_db(SNR(i)));
end

plot(SNR, y, 'linewidth', 4, 'r')
plot(SNR, log(1 + from_db(SNR) * K), 'linewidth', 4, 'b')
V0 = V;

% y = arrayfun(@(snr) MaximumSumRate2(U,H,V .* from_db(snr)), SNR);

for i=1:size(SNR,2)
  [V,U] = maxSINR_algorithm(H, V0, SNR(i), 10);
  y(i) = MaximumSumRate2(U, H, V .* from_db(SNR(i)));
end
plot(SNR, y, 'linewidth', 4, 'k')
