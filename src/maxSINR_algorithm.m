% Max SINR Algorithm for iterative alighnment
%
% Implementation of the Max-SINR algorithm proposed by Gomadam et al.
%
% Usage [V, U] = maxSINR_alignment(H,V,SNRdB,iteration)
%
% 12-02-2014
% Input:
%
% H: MxNxKxK iid gaussian channels
% V: random "precode vector"
% SNRdB: transmit power
% iteration: number of iterations until "converge"




function [V, U] = maxSINR_algorithm(H,V,SNRdB,iteration)

[nRx, nTx,k1,k2] = size(H); % H is [num rec x num tx x user x user];
dk = size(V,2);               % V is [numTx,dk,user] is the degrees of freedom

% start: V[k] : M[k] x d[k], cols(V[k]) lin. indep. unit vectors. & repeat
% until convergence

Power = 10^(SNRdB/10)/(dk); % Power/degrees of freedom (p[k]/d[k]
% Compute B[kl] according to (26) for all k \in {1,2,..,K}, l \in {1,2,..,d[k]}

for ii = 1:iteration;
  
  % Get the U matrix for the Recievers
  for k = 1:k1;
    for d = 1:dk;
      Bkl = 0; %initialize BKL
      %  Equation 26: B[kl] = sum for j \in {1,..,K}((p[j]/d[j])
      % (sum for d \in {1,..,d[j]} H[kj] V[j][*d]V[j][*d]' H[kj]')
      % - (p[k]/d[k])H[kk] V[k][*d]V[k][*k]' H[kk]') + eye(N[k])
      for j = 1:k2;
        for dd = 1:dk;
          Bkl = Bkl+Power*H(:,:,k,j)*V(:,dd,j)*V(:,dd,j)'*H(:,:,k,j)';
        end
        Bkl = Bkl - Power*H(:,:,k,k)*V(:,d,k)*V(:,d,k)'*H(:,:,k,k)';
      end
      Bkl = Bkl + eye(nRx);
      % Equation 27: calculate U[k][*l] according to (27)
      % U[k][*l] = B[kl]^-1 * H[kk] * V[k][*l] / norm(B[kl]^-1 * H[kk] * V[k][*l])
      U(:,d,k) = (Bkl^-1)*H(:,:,k,k)*V(:,d,k);
      U(:,d,k) = U(:,d,k) / norm(U(:,d,k));
    end
  end
  
  % Replace the V with the U,  <-V[k] = U[k]
  for k = 1:k1;
    for d = 1:dk;
      Bkl = 0; %initializee BKL
      %  Equation 26: B[kl] = sum for j \in {1,..,K}((p[j]/d[j])
      % (sum for d \in {1,..,d[j]} H[kj] U[j][*d]U[j][*d]' H[kj]')
      % - (p[k]/d[k])H[kk] U[k][*d]U[k][*k]' H[kk]') + eye(N[k])
      for j = 1:k2;
        for dd = 1:dk;
          Bkl = Bkl+Power*H(:,:,j,k)'*U(:,dd,j)*U(:,dd,j)'*H(:,:,j,k);
        end
        Bkl = Bkl - Power*H(:,:,k,k)'*U(:,d,k)*U(:,d,k)'*H(:,:,k,k);
      end
      Bkl = Bkl + eye(nTx);
      % Equation 27: calculate V[k][*l] according to (27) replaced with U
      % U[k][*l] = B[kl]^-1 * H[kk] * U[k][*l] / norm(B[kl]^-1 * H[kk] * U[k][*l])
      V(:,d,k) = (Bkl^-1)*H(:,:,k,k)'*U(:,d,k)./norm((Bkl^-1)*H(:,:,k,k)'*U(:,d,k));
    end
  end
  
end
end

"Chris Chang" 
