% Iterative Alignment: distributed interference alignment
%
% Implementation of the initial distributed interference alignment proposed
% by Gomadam et al.
%
% Usage: [U,V] = iterative_alignment(M, N, P, H, d)
%
% INPUT:
%
%   M: 1xK vector; number of transmit antennas.
%   N: 1xK vector; number of receive antennas.
%   P: 1xK vector of transmit powers.
%   H: KxK cell array of cross-channel gains.
%      H(i,j) = H(j,i)' is a N(i)xM(j) matrix.
%   d: 1xK vector; number of degrees of freedom.
%   tol: scalar tolerance that determines end of iteration.
%
% OUTPUT:
%
%   V: precoding vectors
%   U: interference suppression vectors

function [V,U] = iterative_alignment(M, N, P, H, d, tol)
K = size(M, 2);

Q = cell(1,K);
V = cell(1,K);
U = cell(1,K);

Qr = cell(1,K); % Xr -> reciprocal channel
Vr = cell(1,K);
Ur = cell(1,K);
Hr = cell(size(H));
for k=1:K
    V{k} = eye(d(k), M(k));
    U{k} = eye(M(k), d(k));
    for j=1:K
        Hr{j,k} = H{k,j}';
        endfor
        endfor
        l = 2 * leakage(P, d, H, U, V);
        % tol
        while abs(l - leakage(P, d, H, U, V)) > tol
            % for a=1:10
            l = leakage(P, d, H, U, V);
            for k=1:K
                Q{k} = interf_cov(k,P,d,H,V{k});
                [v,L] = eig(Q{k});
                
                [S,I] = sort(diag(L));
                U{k}  = v(:,I)(:,1:d(k));
                Vr{k} = U{k};
                endfor
                for k=1:K
                    Qr{k} = interf_cov(k,P,d,Hr,Vr{k});
                    [v,L] = eig(Qr{k});
                    [S,I] = sort(diag(L));
                    Ur{k} = v(:,I)(:,1:d(k));
                    V{k}  = Ur{k};
                end
            end
        end
    end
    
    function Q = interf_cov(k, P, d, H, V)
    K = size(P,2);
    Q = zeros(size(H{k,1}, 1));
    for j=[1:k-1,k+1:K]
        Q = Q + P(j) / d(j) * H{k,j}*(V*V' * H{k,j}');
    end
    end
    
    function Iw = leakage(P, d, H, U, V)
    K = size(P,2);
    Iw = 0;
    for k=1:K
        Q = interf_cov(k,P,d,H,V{k});
        Iwk = trace(U{k}' * Q * U{k});
        Iw = Iw + real(Iwk);
    end
    end
    
