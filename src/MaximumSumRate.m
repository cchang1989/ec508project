% Calculate the Maximum Sum Rate
% Source
% http://www.researchgate.net/publication/221286207_Maximum_Sum-Rate_Interference_Alignment_Algorithms_for_MIMO_Channels


function R = MaximumSumRate(U, H, V)

R = 0;
[~, dk K] = size(V); % [numTx,dk,user]

S = zeros(dk,dk,K); % initialize the useful signal matrix
J = zeros(dk, (K-1)*dk, K); % initialize the interference matrix

% R = sum from {1:K} |log(I + (sigma^2*I)+sum(Qkl))^-1*Qkk)| from 3 %similar to log(1+SINR)?
% R = sum from {1:K} det(log(I+1/sig^2 * Uk^H * Hkk * Vk * Vk^H *Hkk^H *Uk))
for k = 1:K
    UkH = (eye(size(U,1))+U(:,:,k))\eye(size(U,1)); % Singular matrix????
    Hkk = H(:,:,k,k);
    Vk = V(:,:,k);
    VkH = (eye(size(U,1))+Vk)\eye(size(V,1));
    HkkH = (eye(size(U,1))+Hkk)\eye(size(Hkk,1));
    Uk = U(:,:,k);
    
    UkH*Uk
    VkH*Vk
    
    sigma = 1; % for now leave noise as a guassian with variance 1
    R = R + det(log(eye(size(U,1))+(1/sigma^2)*UkH*Hkk*Vk*VkH*HkkH*Uk));
end

end

