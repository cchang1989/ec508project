% Testing Script

clc, clear all, close all

%% Testing

MC = 2; % number of MCs
K = 2;  % number of users
dk = 3; % degrees of freedom
M = 3; % Tx
N = 3; % Rx
numiter = 100; % number of iterations to "converge"
SNRdB = 0; % transmit power

H = randn(N,M,K,K);   % generate gaussian i.i.d channels
V = randn(M,dk,K); % V normalized random precode
for ii = 1:K
    V(:,:,ii) = V(:,:,ii)./norm(V(:,:,ii)); % normalize X(:,:,i)
end

[V, U] = maxSINR_algorithm(H,V,SNRdB,numiter);

