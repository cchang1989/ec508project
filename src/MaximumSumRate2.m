function R = MaximumSumRate2(U, H, V)
R = 0;
[~, dk K] = size(V); % [numTx,dk,user]
for k = 1:K
    % Signal
    SNR(:,:,k) = U(:,:,k)'*H(:,:,k,k)*V(:,:,k); % construct the signal space matrix    
    % Interference Matrix
    % U is the reciever
    % V is the transmitter
    for l = [1:k-1 k+1:K] % everything but k, interference occurs from the other corresponding attenna
      Interference(:,:,k) = U(:,:,k)'*H(:,:,k,l)*V(:,:,l);
    end
    R = R + .5*log2(det(eye(size(Interference,1))+(eye(size(Interference,1))+Interference(:,:,k)*Interference(:,:,k)')^-1*SNR(:,:,k)*SNR(:,:,k)')); % calculate the rate
end
end

